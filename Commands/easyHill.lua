function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "listOfUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.pos = nil
end

function Run(self, units, parameter)
	self.threshold = 100

	local listOfUnits = parameter.listOfUnits -- table
   	-- local position = parameter.position -- Vec3

	local hardCodedPositions = {{1375, 300}, {1950, 800}, {2600, 360}}
    

	if self.pos == nil then
		self.pos = {}
		self.pos[1] = Vec3(hardCodedPositions[1][1], 0, hardCodedPositions[1][2])
		SpringGiveOrderToUnit(listOfUnits[1], CMD.MOVE, self.pos[1]:AsSpringVector(), {})
		self.pos[2] = Vec3(hardCodedPositions[2][1], 0, hardCodedPositions[2][2])
		SpringGiveOrderToUnit(listOfUnits[2], CMD.MOVE, self.pos[2]:AsSpringVector(), {})
		self.pos[3] = Vec3(hardCodedPositions[3][1], 0, hardCodedPositions[3][2])
		SpringGiveOrderToUnit(listOfUnits[3], CMD.MOVE, self.pos[3]:AsSpringVector(), {})
	end

	for i=1, 4 do
		local pointX, pointY, pointZ = SpringGetUnitPosition(listOfUnits[i]) 
		local pointmanPosition = Vec3(pointX, pointY, pointZ)
		if (pointmanPosition:Distance(self.pos[i]) > self.threshold) then
			return RUNNING
		end
	end
	
	
	return SUCCESS
	
end


function Reset(self)
	ClearState(self)
end
